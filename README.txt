
-- SUMMARY --

The Distance Cataloging Interface (DCI) was built as a way to collect and review feedback on specified content on a Drupal website from it's users.

For a full description of the module, visit the project page:
  http://drupal.org/project/dci

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/dci


-- REQUIREMENTS --

This module requires the following contributed modules:
* date - https://www.drupal.org/project/date
* entity - https://www.drupal.org/project/entity
* entityreference - https://www.drupal.org/project/entityreference


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Administer Distance Cataloging Interface (DCI module)
  
    This permission is required to access DCI admin dashboard, content review dashboard and other DCI admin tools.

  - Submit content feedback (DCI module)
  
    This permission allows submitting DCI feedback for content that is assigned to a user.

* Enable DCI functionality for one or more content types in Administration » Structure » Content Types. Edit the desired content type to enable DCI in "DCI Settings" vertical tab.

* Additional DCI feedback fields can be added by editing a DCI-enabled content type and selecting DCI Feedback tab.

* Additional DCI group fields can be added in Administration » DCI » Groups » Manage Fields.


-- FAQ --

Q: I enabled the DCI for my content type but the feedback form isn't showing up. Did I do something wrong?

A: Not at all! The feedback form will only appear if the node has been assigned to your user account by an administrator. 


-- CONTACT --

Current maintainers:
* Oleksandr Grygorash (ogrygorash) - https://www.drupal.org/u/ogrygorash

This project has been sponsored by:
* Gilcrease Museum - https://gilcrease.org
  The Thomas Gilcrease Institute of American History and Art, commonly known as Gilcrease Museum, located in Tulsa, Okla., is one of the country’s best facilities for the preservation and study of American art and history.

* ILMS - https://www.imls.gov/

* Urban Insight - https://urbaninsight.com
  Established in 1997, Urban Insight is the largest Drupal consulting firm in the Los Angeles area. We plan, build and operate successful websites for clients across the country. We have a long history of advocating and using open source software for our clients.