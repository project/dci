<?php

/**
 * @file
 * Views handler to display the DCI status of a node.
 */

/**
 * Field handler to present the DCI status of a node to the user.
 */
class dci_handler_field_dci_status extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
    $this->field_alias = $this->table . '_' . $this->field;
  }

  function pre_render(&$values) {

    $nids = array();
    $ids = array();
    foreach ($values as $id => $result) {
      $nids[] = $result->{$this->aliases['nid']};
      $values[$id]->{$this->field_alias} = 0;
      // Create a reference so we can find this record in the values again.
      if (empty($ids[$result->{$this->aliases['nid']}])) {
        $ids[$result->{$this->aliases['nid']}] = array();
      }
      $ids[$result->{$this->aliases['nid']}][] = $id;
    }

    if ($nids) {
      $result = db_query("SELECT nid, COUNT(nid) as num_comments FROM {dci_feedback} f WHERE f.nid IN (:nids) GROUP BY f.nid", array(
          ':nids' => $nids,
        ));

      foreach ($result as $node) {
        foreach ($ids[$node->nid] as $id) {
          $values[$id]->{$this->field_alias} = 1;
        }
      }
    }
  }

  function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      return $value;
    }
    else {
      return NULL;
    }
  }
}
