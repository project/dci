<?php

function dci_views_data_alter(&$data) {

  $data['dci_group']['due']['field']['handler'] = 'views_handler_field_date';
  $data['dci_group']['due']['sort']['handler'] = 'views_handler_sort_date';
  $data['dci_group']['due']['filter']['handler'] = 'views_handler_filter_date';
  
  $data['node']['dci_status'] = array(
    'title' => t('DCI Status'),
    'help' => t('DCI status of a node'),
    'field' => array(
      'handler' => 'dci_handler_field_dci_status',
    ),
  );
}