<?php

/**
 * Menu callback; presents the DCI group editing form.
 */
function dci_group_edit($group) {
  
  drupal_set_title(t('<em>Edit</em> @title', array('@title' => $group->title)), PASS_THROUGH);
  return drupal_get_form('dci_group_form', $group);
}

/**
 * Returns a node submission form.
 *
 * @param $type
 *   The node type for the submitted node.
 *
 * @return
 *   The themed form.
 */
function dci_group_add() {
  global $user;

  $dci_group = (object) array('gid' => NULL, 'uid' => $user->uid, 'title' => NULL, 'due' => NULL, 'created' => REQUEST_TIME);
  drupal_set_title(t('Create DCI Group'));
  $output = drupal_get_form('dci_group_form', $dci_group);

  return $output;
}

/**
 * Form constructor for the group add/edit form.
 */
function dci_group_form($form, &$form_state, $group) {
  global $user;

  // During initial form build, add the DCI group entity to the form state for
  // use during form building and processing. During a rebuild, use what is in
  // the form state.
  if (!isset($form_state['dci_group'])) {
    $defaults = array(
      'gid' => NULL,
      'uid' => 0,
      'title' => NULL,
      'due' => NULL,
    );
    foreach ($defaults as $key => $value) {
      if (!isset($group->$key)) {
        $group->$key = $value;
      }
    }
    $form_state['dci_group'] = $group;
  }
  else {
    $group = $form_state['dci_group'];
  }

  $group->uid = $user->uid;

  if (!empty($group->due)) {
    $group->date = format_date($group->due, 'custom', 'Y-m-d H:i:s O');
  }

  $form['#attributes']['class'][] = 'dci-group-form';
  $form['#theme'] = array('dci_group_form');

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Group Name'),
    '#required' => TRUE,
    '#default_value' => $group->title,
    '#maxlength' => 255,
    '#weight' => 0,
  );

  $form['date'] = array(
    '#type' => 'textfield',
    '#title' => t('Due Date'),
    '#required' => TRUE,
    '#description' => t('Format: %time. The date format is YYYY-MM-DD and %timezone is the time zone offset from UTC.', array('%time' => !empty($group->date) ? date_format(date_create($group->date), 'Y-m-d H:i:s O') : format_date($group->created, 'custom', 'Y-m-d H:i:s O'), '%timezone' => !empty($group->date) ? date_format(date_create($group->date), 'O') : format_date($group->created, 'custom', 'O'))),
    '#default_value' => empty($group->date) ? '' : $group->date,
    '#maxlength' => 25,
    '#weight' => 1,
  );

  // Add internal group properties.
  foreach (array('gid', 'uid') as $key) {
    $form[$key] = array('#type' => 'value', '#value' => $group->$key);
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#access' => TRUE,
    '#weight' => 15,
  );  


  if (!empty($group->gid)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 20,
      '#submit' => array('dci_group_form_delete_submit'),
    );
  }

  // Attach fields.
  field_attach_form('dci_group', $group, $form, $form_state);

  return $form;
}

/**
 * Form validation handler for dci_group_form().
 *
 * @see dci_group_form()
 * @see dci_group_form_submit()
 */
function dci_group_form_validate($form, &$form_state) {
  // $form_state['group'] contains the actual entity being edited, but we must
  // not update it with form values that have not yet been validated, so we
  // create a pseudo-entity to use during validation.
  $group = (object) $form_state['values'];

  // Validate the "due date" field.
  if (!empty($group->date) && strtotime($group->date) === FALSE) {
    form_set_error('date', t('You have to specify a valid date.'));
  }
  
  entity_form_field_validate('dci_group', $form, $form_state);
}

/**
 * Form submission handler for dci_group_form().
 *
 * Handles the 'Delete' button on the DCI group form.
 *
 */
function dci_group_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $group = $form_state['dci_group'];
  $form_state['redirect'] = array('admin/dci/groups/' . $group->gid . '/delete', array('query' => $destination));
}


/**
 * Prepare a DCI Group for submission.
 */
function dci_group_submit($dci_group) {

  if (!empty($dci_group->date)) {
    $dci_group->due = strtotime($dci_group->date);
  }
  if (empty($dci_group->created)) {
    $dci_group->created = REQUEST_TIME;
  }
  $dci_group->changed = REQUEST_TIME;  
 
  return $dci_group;
}

/**
 * Updates the form state's DCI Group entity by processing this submission's values.
 *
 * This is the default builder function for the DCI Group form. It is called
 * during the "Save" submit handlers to retrieve the entity to save.
 *
 * @see dci_group_form()
 */
function dci_group_form_submit_build_group($form, &$form_state) {
  $dci_group = $form_state['dci_group'];
  entity_form_submit_build_entity('dci_group', $dci_group, $form, $form_state);
  dci_group_submit($dci_group);
  return $dci_group;
}

/**
 * Process DCI Group form submissions; prepare the group, store it, and set a redirection target.
 */
function dci_group_form_submit($form, &$form_state) {
  
  $group = dci_group_form_submit_build_group($form, $form_state);

  if (user_access('administer dci')) {

    dci_group_save($group);
    $form_state['values']['gid'] = $group->gid;

    // Add an entry to the watchdog log.
    watchdog('dci', 'DCI Group %title saved.', array('%title' => $group->title), WATCHDOG_NOTICE);

    // Explain the approval queue if necessary.
    drupal_set_message(t('Group %title has been saved.', array('%title' => $group->title)));
  }

  $form_state['redirect'] = 'admin/dci/groups';
}

/**
 * Form constructor for the DCI group deletion confirmation form.
 *
 * @see dci_group_delete_confirm_submit()
 */
function dci_group_delete_confirm($form, &$form_state, $dci_group) {
  
  // Always provide entity id in the same form key as in the entity edit form.
  $form['gid'] = array('#type' => 'value', '#value' => $dci_group->gid);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $dci_group->title)),
    'admin/dci/groups/' . $dci_group->gid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes DCI group deletion.
 *
 * @see dci_group_delete_confirm()
 */
function dci_group_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $group = dci_group_load($form_state['values']['gid']);
    dci_group_delete($form_state['values']['gid']);
    watchdog('dci', 'DCI Group: deleted %title.', array('%title' => $group->title));
    drupal_set_message(t('DCI Group %title has been deleted.', array('%title' => $group->title)));
  }

  $form_state['redirect'] = '<front>';
}

/**
 * Form constructor for the feedback status change confirmation form.
 *
 * @see dci_feedback_set_status_confirm_submit()
 */
function dci_feedback_set_status_confirm($form, &$form_state, $feedback, $status) {
  $form['#feedback'] = $feedback;

  $node = node_load($feedback->nid);

  $options = array(
    'reset' => array(
      'action' => t('Reset'),
      'operation' => t('reset moderation status of the feedback'),
      'description' => '',
    ),
    'reject' => array(
      'action' => t('Reject'),
      'operation' => t('reject the feedback'),
      'description' => '',
    ),
    'approve' => array(
      'action' => t('Approve'),
      'operation' => t('approve the feedback'),
      'description' => '',
    ),
  );

  // Always provide entity id in the same form key as in the entity edit form.
  $form['fid'] = array('#type' => 'value', '#value' => $feedback->fid);
  $form['status'] = array('#type' => 'value', '#value' => $status);
  return confirm_form($form,
    t('Are you sure you want to @operation for %title?', array('@operation' => $options[$status]['operation'], '%title' => $node->title)),
    'admin/dci/review',
    $options[$status]['description'],
    $options[$status]['action'],
    t('Cancel')
  );
}


/**
 * Executes feedback status change.
 *
 * @see dci_feedback_set_status_confirm()
 */
function dci_feedback_set_status_confirm_submit($form, &$form_state) {
  
  if ($form_state['values']['confirm']) {

    $status = array(
      'reset' => array(
        'value' => NULL,
        'operation' => t('reset'),
      ),
      'reject' => array(
        'value' => 0,
        'operation' => t('rejected'),
      ),
      'approve' => array(
        'value' => 1,
        'operation' => t('approved'),
      ),
    );    

    $feedback = dci_feedback_load($form_state['values']['fid']);
    $node = node_load($feedback->nid);
    $feedback->status = $status[$form_state['values']['status']]['value'];
    $feedback->reviewed = is_null($feedback->status) ? NULL : REQUEST_TIME;
    dci_feedback_save($feedback);

    $operation = $status[$form_state['values']['status']]['operation'];

    watchdog('dci', 'Feedback for @type: %title has been @operation.', array('@type' => $node->type, '%title' => $node->title, '@operation' => $operation));
    drupal_set_message(t('Feedback for @type %title has been @operation.', array('@type' => node_type_get_name($node), '%title' => $node->title, '@operation' => $operation)));
  }

  $form_state['redirect'] = 'admin/dci/review';
}

function dci_user_dashboard_page($account) {

  drupal_set_title(t('DCI User Dashboard'));
  
  $build = dci_build_user_dashboard($account);
  
  return $build;
}

function dci_build_user_dashboard($account) {

  $dci_types = dci_node_types();

  $query = db_select('node', 'n');
  $query->addTag('dci_content');
  $query->join('field_data_dci_nodes', 'fn', 'fn.dci_nodes_target_id = n.nid');
  $query->join('field_data_dci_users', 'fu', 'fu.entity_id = fn.entity_id');
  $query->join('dci_group', 'g', 'g.gid = fn.entity_id');
  $query->leftJoin('dci_feedback', 'f', 'fn.dci_nodes_target_id = f.nid AND fu.dci_users_target_id = f.uid');
  $query->condition('n.type', $dci_types, 'IN');
  $query->condition('fu.dci_users_target_id', $account->uid);
  $query->addExpression('COUNT(*)', 'total');
  $query->addExpression('COUNT(f.fid)', 'completed');

  $result = $query->execute()->fetchObject();
  $total = $result->total > 0 ? min(100, intval($result->completed * 100 / $result->total)) : ($result->completed > 0 ? 100 : 0);

  $query = db_select('node', 'n');
  $query->addTag('dci_content');
  $query->join('field_data_dci_nodes', 'fn', 'fn.dci_nodes_target_id = n.nid');
  $query->join('field_data_dci_users', 'fu', 'fu.entity_id = fn.entity_id');
  $query->leftJoin('dci_group', 'g', 'g.gid = fn.entity_id AND g.due < UNIX_TIMESTAMP(DATE_ADD(DATE_SUB(CURDATE(), INTERVAL DAYOFMONTH(CURDATE()) - 1 DAY), INTERVAL 1 MONTH))');
  $query->leftJoin('dci_feedback', 'f', 'fn.dci_nodes_target_id = f.nid AND fu.dci_users_target_id = f.uid');
  $query->condition('n.type', $dci_types, 'IN');
  $query->condition('fu.dci_users_target_id', $account->uid);
  $query->addExpression('COUNT(g.gid)', 'total');
  $query->addExpression('COUNT(f.fid)', 'completed');

  $result = $query->execute()->fetchObject();
  $month = $result->total > 0 ? min(100, intval($result->completed * 100 / $result->total)) : ($result->completed > 0 ? 100 : 0);

  $query = db_select('node', 'n');
  $query->addTag('dci_content');
  $query->join('field_data_dci_nodes', 'fn', 'fn.dci_nodes_target_id = n.nid');
  $query->join('field_data_dci_users', 'fu', 'fu.entity_id = fn.entity_id');
  $query->leftJoin('dci_group', 'g', 'g.gid = fn.entity_id AND g.due < UNIX_TIMESTAMP(DATE_ADD(DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) DAY), INTERVAL 1 WEEK))');
  $query->leftJoin('dci_feedback', 'f', 'fn.dci_nodes_target_id = f.nid AND fu.dci_users_target_id = f.uid');
  $query->condition('n.type', $dci_types, 'IN');
  $query->condition('fu.dci_users_target_id', $account->uid);
  $query->addExpression('COUNT(g.gid)', 'total');
  $query->addExpression('COUNT(f.fid)', 'completed');

  $result = $query->execute()->fetchObject();
  $week = $result->total > 0 ? min(100, intval($result->completed * 100 / $result->total)) : ($result->completed > 0 ? 100 : 0);

  $profile = array();

  if (!empty($account->picture)) {
    $profile['picture'] = array(
      '#theme' => 'user_picture',
      '#account' => $account,
      '#weight' => 0,
    );
  }

  $profile['name'] = array(
    '#theme' => 'username',
    '#account' => $account,
    '#weight' => 1,
  );

  $progress['title'] = array(
    '#type' => 'markup',
    '#markup' => t('Progress'),
  );
  $progress['week'] = array(
    '#type' => 'markup',
    '#markup' => $week . '%',
    '#value' => $week,
  );
  $progress['month'] = array(
    '#type' => 'markup',
    '#markup' => $month . '%',
    '#value' => $month,
  );
  $progress['total'] = array(
    '#type' => 'markup',
    '#markup' => $total . '%',
    '#value' => $total,
  );

  $content = array(
    '#weight' => 100,
    'nodes' => _dci_user_dashboard_nodes($account),
  );  
  
  $build = array(
    '#theme' => 'dci_user_dashboard',
    '#account' => $account,
    'profile' => $profile,
    'progress' => $progress,
    'content' => $content,    
  );
  
  return $build;
}


function _dci_user_dashboard_nodes($account) {

  $destination = drupal_get_destination();
  $dci_types = dci_node_types();
  
  // Build the sortable table header.
  $header = array(
    'title' => t('Title'),
    'type' => t('Type'),
    'author' => t('Author'),
    'changed' => t('Updated'),
    'due' => t('Due'),
    'status' => t('Status'),
  );

  $query = db_select('node', 'n')->extend('PagerDefault')->extend('TableSort');
  $query->addTag('dci_content');
  $query->join('field_data_dci_nodes', 'fn', 'fn.dci_nodes_target_id = n.nid');
  $query->join('field_data_dci_users', 'fu', 'fu.entity_id = fn.entity_id');
  $query->join('dci_group', 'g', 'g.gid = fn.entity_id');
  $query->addJoin('LEFT OUTER', 'dci_feedback', 'f', 'fn.dci_nodes_target_id = f.nid AND fu.dci_users_target_id = f.uid');
  $query->condition('fu.dci_users_target_id', $account->uid);
  $query->condition('n.type', $dci_types, 'IN');
  $query->fields('n', array('nid', 'title', 'changed', 'type', 'uid'));
  $query->fields('g', array('due'));
  $query->addExpression("CASE WHEN f.fid IS NULL THEN 'pending' ELSE 'completed' END", 'status');
  $query->orderBy('g.due', 'asc');
  $query->limit(10);
  
  $nodes = $query->execute();

  // Prepare the list of nodes.
  $options = array();
  foreach ($nodes as $node) {
    $options[$node->nid] = array(
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $node->title,
          '#href' => 'node/' . $node->nid . '/dci',
          '#options' => array('query' => $destination),
        ),
      ),
      'type' => check_plain(node_type_get_name($node)),
      'author' => theme('username', array('account' => $node)),      
      'changed' => format_date($node->changed, 'short'),
      'due' => format_date($node->due, 'custom', 'm/d/Y'),
      'status' => $node->status,
    );
  }

  $form['nodes'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No content available.'),
  );

  $form['pager'] = array('#theme' => 'pager');

  return $form;
}