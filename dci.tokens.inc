<?php

/**
 * @file
 * Builds placeholder replacement tokens for dci-related data.
 */

/**
 * Implements hook_token_info().
 */
function dci_token_info() {
  $type = array(
    'name' => t('DCI Groups'),
    'description' => t('Tokens related to individual DCI groups.'),
    'needs-data' => 'dci_group',
  );

  // Core tokens for DCI groups.
  $dci_group['gid'] = array(
    'name' => t("DCI Group ID"),
    'description' => t('The unique ID of the DCI group.'),
  );
  $dci_group['title'] = array(
    'name' => t("Title"),
    'description' => t("The title of the DCI group."),
  );
  // Chained tokens for DCI groups.
  $dci_group['created'] = array(
    'name' => t("Date created"),
    'description' => t("The date the DCI group was posted."),
    'type' => 'date',
  );
  $dci_group['changed'] = array(
    'name' => t("Date changed"),
    'description' => t("The date the DCI group was most recently updated."),
    'type' => 'date',
  );
  $dci_group['due'] = array(
    'name' => t("Date due"),
    'description' => t("The date the DCI group is due."),
    'type' => 'date',
  );
  
  return array(
    'types' => array('dci_group' => $type),
    'tokens' => array('dci_group' => $dci_group),
  );
}

/**
 * Implements hook_tokens().
 */
function dci_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'dci_group' && !empty($data['dci_group'])) {
    $dci_group = $data['dci_group'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the DCI group.
        case 'gid':
          $replacements[$original] = $dci_group->gid;
          break;

        case 'title':
          $replacements[$original] = $sanitize ? check_plain($dci_group->title) : $dci_group->title;
          break;

        case 'created':
          $replacements[$original] = format_date($dci_group->created, 'medium', '', NULL, $language_code);
          break;

        case 'changed':
          $replacements[$original] = format_date($dci_group->changed, 'medium', '', NULL, $language_code);
          break;

        case 'due':
          $replacements[$original] = format_date($dci_group->due, 'custom', 'm/d/Y', NULL, $language_code);
          break;
      }
    }
  }

  return $replacements;
}
