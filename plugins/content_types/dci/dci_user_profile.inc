<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('DCI User Profile'),
  'description' => t('The DCI profile of a user.'),
  'required context' => new ctools_context_required(t('User'), 'user'),
  'category' => t('DCI'),
  'defaults' => array(),
);

/**
 * Render the user profile content type.
 */
function dci_dci_user_profile_content_type_render($subtype, $conf, $panel_args, $context) {
  $account = isset($context->data) ? clone($context->data) : NULL;
  if (!$account) {
    return NULL;
  }

  global $user;

  $build = array();

  if ($user->uid == $account->uid && user_access('submit content feedback', $account)) {
    $build = array(
      '#theme' => 'dci_user_profile_block',
      '#account' => $account,
    );
  }

  $block = new stdClass();
  $block->module = 'dci-user-profile';
  $block->content = $build;

  return $block;
}

/**
 * Display the administrative title for a panel pane in the drag & drop UI.
 */
function dci_dci_user_profile_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" DCI user profile', array('@s' => $context->identifier));
}

function dci_dci_user_profile_content_type_edit_form($form, &$form_state) {

  return $form;
}

function dci_dci_user_profile_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['view_mode'] = $form_state['values']['view_mode'];
}

