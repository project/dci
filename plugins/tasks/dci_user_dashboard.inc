<?php

/**
 * @file
 * Overrides the user profile display at user/%user.
 *
 * Specialized implementation of hook_page_manager_task_tasks(). See api-task.html for
 * more information.
 */
function dci_dci_user_dashboard_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',
    'title' => t('DCI User dashboard template'),
    'admin title' => t('DCI User dashboard template'),
    'admin description' => t('When enabled, this overrides the default Drupal behavior for displaying DCI user dashboard at <em>user/%user/dci</em>.'),
    'admin path' => 'user/%user/dci',

    // Callback to add items to the page managertask administration form:
    'task admin' => 'dci_dci_user_dashboard_task_admin',

    'hook menu' => 'dci_dci_user_dashboard_menu',
    'hook menu alter' => 'dci_dci_user_dashboard_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context', // handler type -- misnamed
    'get arguments' => 'dci_dci_user_dashboard_get_arguments',
    'get context placeholders' => 'dci_dci_user_dashboard_get_contexts',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('dci_dci_user_dashboard_disabled', TRUE),
    'enable callback' => 'dci_dci_user_dashboard_enable',
    'access callback' => 'dci_dci_user_dashboard_access_check',
  );
}

/**
 * Callback defined by dci_dci_user_dashboard_page_manager_tasks().
 *
 */
function dci_dci_user_dashboard_menu_alter(&$items, $task) {
  if (variable_get('dci_dci_user_dashboard_disabled', TRUE)) {
    return;
  }

  // Override the user view handler for our purpose.
  if ($items['user/%user/dci']['page callback'] == 'dci_user_dashboard_page' || variable_get('page_manager_override_anyway', FALSE)) {
    $items['user/%user/dci']['page callback'] = 'dci_dci_user_dashboard_page';
    $items['user/%user/dci']['file path'] = $task['path'];
    $items['user/%user/dci']['file'] = $task['file'];
  }
  else {
    // automatically disable this task if it cannot be enabled.
    variable_set('dci_dci_user_dashboard_disabled', TRUE);
    if (!empty($GLOBALS['page_manager_enabling_dci_user_dashboard'])) {
      drupal_set_message(t('Page manager module is unable to enable user/%user/dci because some other module already has overridden with %callback.', array('%callback' => $items['user/%user/dci']['page callback'])), 'error');
    }
  }
}

/**
 * Entry point for our overridden DCI user dashboard.
 *
 * This function asks its assigned handlers who, if anyone, would like
 * to run with it. If no one does, it passes through to Drupal core's
 * user view, which is user_page_view().
 */
function dci_dci_user_dashboard_page($account) {
  // Load my task plugin:
  $task = page_manager_get_task('dci_user_dashboard');

  // Load the account into a context.
  ctools_include('context');
  ctools_include('context-task-handler');
  $contexts = ctools_context_handler_get_task_contexts($task, '', array($account));

  $output = ctools_context_handler_render($task, '', $contexts, array($account->uid));
  if ($output !== FALSE) {
    return $output;
  }

  module_load_include('inc', 'dci', 'dci.pages');
  $default_output = dci_user_dashboard_page($account);

  // Fall back to the default output generated by dci_user_dashboard_page().
  return $default_output;
}

/**
 * Callback to get arguments provided by this task handler.
 *
 * Since this is the node view and there is no UI on the arguments, we
 * create dummy arguments that contain the needed data.
 */
function dci_dci_user_dashboard_get_arguments($task, $subtask_id) {
  return array(
    array(
      'keyword' => 'user',
      'identifier' => t('User being viewed'),
      'id' => 1,
      'name' => 'entity_id:user',
      'settings' => array(),
    ),
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function dci_dci_user_dashboard_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(dci_dci_user_dashboard_get_arguments($task, $subtask_id));
}

/**
 * Callback to enable/disable the page from the UI.
 */
function dci_dci_user_dashboard_enable($cache, $status) {
  variable_set('dci_dci_user_dashboard_disabled', $status);

  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['page_manager_enabling_dci_user_dashboard'] = TRUE;
  }
}

/**
 * Callback to determine if a page is accessible.
 *
 * @param $task
 *   The task plugin.
 * @param $subtask_id
 *   The subtask id
 * @param $contexts
 *   The contexts loaded for the task.
 * @return
 *   TRUE if the current user can access the page.
 */
function dci_dci_user_dashboard_access_check($task, $subtask_id, $contexts) {
  $context = reset($contexts);
  return user_view_access($context->data);
}
