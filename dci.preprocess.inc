<?php

function template_preprocess_dci_admin_dashboard(&$vars) {

  foreach (element_children($vars['elements']) as $key) {
    $vars[$key] = $vars['elements'][$key];
  }
}

function template_preprocess_dci_admin_dashboard_block(&$vars) {
  
  foreach (element_children($vars['elements']) as $key) {
    $vars[$key] = $vars['elements'][$key];
  }
}

function template_preprocess_dci_admin_groups(&$vars) {

  foreach (element_children($vars['elements']) as $key) {
    $vars[$key] = $vars['elements'][$key];
  }
}

function template_preprocess_dci_admin_content_review(&$vars) {

  foreach (element_children($vars['elements']) as $key) {
    $vars[$key] = $vars['elements'][$key];
  }
}

function template_preprocess_dci_user_profile_block(&$vars) {

  $account = $vars['account'];
  
  $vars['content'] = array();

  $vars['content']['links'] = array(
    '#theme' => 'links',
    '#links' => array(
      array(
        'title' => t('Visit Dashboard'),
        'href' => 'user/' . $account->uid . '/dci',
      ),
      array(
        'title' => t('View Pending'),
        'href' => 'user/' . $account->uid . '/dci/pending',
      ),
    ),
    '#attributes' => array('class' => array('links', 'inline')),
  );

  $next_nid = dci_get_next_pending_node();
  if (!empty($next_nid)) {
    $vars['content']['links']['#links'][] = array(
      'title' => t('Next Object'),
      'href' => 'node/' . $next_nid . '/dci',
    );
  }

  $vars['content']['nodes'] = _dci_user_profile_block_nodes($account);
}


function _dci_user_profile_block_nodes($account) {

  $destination = drupal_get_destination();
  $dci_types = dci_node_types();
  
  // Build the sortable table header.
  $header = array(
    'title' => t('Title'),
    'type' => t('Type'),
    'author' => t('Author'),
    'changed' => t('Updated'),
    'status' => t('Status'),
  );

  $nodes = array();

  if (!empty($dci_types)) {
    $query = db_select('node', 'n');
    $query->addTag('dci_content');
    $query->join('field_data_dci_nodes', 'fn', 'fn.dci_nodes_target_id = n.nid');
    $query->join('field_data_dci_users', 'fu', 'fu.entity_id = fn.entity_id');
    $query->addJoin('LEFT OUTER', 'dci_feedback', 'f', 'fn.dci_nodes_target_id = f.nid AND fu.dci_users_target_id = f.uid');
    $query->condition('fu.dci_users_target_id', $account->uid);
    $query->condition('n.type', $dci_types, 'IN');
    $query->fields('n', array('nid', 'title', 'changed', 'type', 'uid'));
    $query->addExpression("CASE WHEN f.fid IS NULL THEN 'pending' ELSE 'completed' END", 'status');
    $query->orderBy('n.changed', 'desc');
    $query->range(0, 10);
    
    $nodes = $query->execute();
  }

  // Prepare the list of nodes.
  $options = array();
  foreach ($nodes as $node) {
    $options[$node->nid] = array(
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $node->title,
          '#href' => 'node/' . $node->nid . '/dci',
          '#options' => array('query' => $destination),
        ),
      ),
      'type' => check_plain(node_type_get_name($node)),
      'author' => theme('username', array('account' => $node)),      
      'changed' => format_date($node->changed, 'short'),
      'status' => $node->status,
    );
  }

  $form['nodes'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No content available.'),
  );

  return $form;
}

function template_preprocess_dci_user_dashboard(&$vars) {

  foreach (element_children($vars['elements']) as $key) {
    $vars['dashboard'][$key] = $vars['elements'][$key];
  }
}