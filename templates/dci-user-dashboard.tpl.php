<div class="dci-user-dashboard"<?php print $attributes; ?>>
  <div class="dashboard-header">
    <div>
      <?php print render($dashboard['profile']); ?>
    </div>
    <div>
      <div>
        <h4>
          <?php print render($dashboard['progress']['title']); ?>
        </h4>
      </div>
      <div>
        <div>
          <span><?php print render($dashboard['progress']['week']); ?></span>
          <div>This Week</div>          
        </div>
        <div>
          <span><?php print render($dashboard['progress']['month']); ?></span>
          <div>This Month</div>          
        </div>
        <div>
          <span><?php print render($dashboard['progress']['total']); ?></span>
          <div>Overall Progress</div>          
        </div>
      </div>
    </div>
  </div>
  <div class="dashboard-header">
    <?php print render($dashboard['content']); ?>
  </div>
</div>
