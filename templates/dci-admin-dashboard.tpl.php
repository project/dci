<div class="admin dci-admin-dashboard clearfix">
  <div class="dashboard-left left clearfix">
    <?php print render($left); ?>
  </div>

  <div class="dashboard-right right clearfix">
    <?php print render($right); ?>
  </div>
</div>
