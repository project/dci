<div class="admin-panel dashboard-block">
  <h3 class="dashboard-title"><?php print render($title); ?></h3>
  <div class="dashboard-content">
    <?php print render($content); ?>    
  </div>
</div>
